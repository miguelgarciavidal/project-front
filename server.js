var express = require('express'),
  app = express(),
  port = process.env.PORT || 8081;

var path = require('path');
//Se indica el index de la versión
app.use(express.static(__dirname + '/build/default'))
app.listen(port);

console.log('Ejecutando Polymer desde Node por el puerto: ' + port);

app.get ('/',function(req,res) {
  //Se indica que se enviará el index.html de la raiz
  res.sendFile("index.html",{root: '.'});
})
